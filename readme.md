# SCRAT Sonde

- **But :** Sonde d'analyse de trafic réseau.
- **Environnement de déploiement :** Raspberry Pi OS

**Modules**
| Nom | But | Etat |
| :--: | :--: | :--: |
| app_web | IHM de configuration et de visulaition des alertes/KPI | ✅ |
| app_capture | Capture réseau |  ✅ |
| app_ipmac | Analyse des IP/MAC| ✅ |
| app_protocol | Analyse des ports | ✅ |
| app_??? | Analyse des IOC | ❌ |
| app_??? | Analyse des comportements | ❌ |
| app_??? | Stockage des données en BDD | ❌ |




## Get started

1. Préparation de la carte SD [Hostname: scrat] : https://www.raspberrypi.com/software/
2. Allimentation de la RPI + connexion ssh
3. Lancement de la commande suivante avec l'utilisateur **root**.
```bash
date -s '2023-06-20  14:00:00' \
&& wget https://gitlab.com/Adrien-/sonde_scrat/-/archive/master/sonde_scrat-master.tar.gz \
&& sleep 2 \
&& tar -xf sonde_scrat-master.tar.gz \
&& rm sonde_scrat-master.tar.gz \
&& mv sonde_scrat-* sonde_scrat \
&& cd sonde_scrat \
&& chmod +x setup.sh \
&& ./setup.sh
```
4. Ouvrir un navigateur web a l'adresse suivante `<IP_RPI:5000>` pour acceder a l'IHM
5. Saisir un nom pour l'analyse + cliquer sur le bouton `Lancer l'analyse`

## Architecture
```mermaid
flowchart LR
    A(<h2>START RPI</h2> <br/> Demarrage de la RPI <br/> Start auto service_web) -- Ouvertire du navigateur web--> D(<h2>START SCRAT</h2> Acces web IHM <br/> Configuration par l'utilisateur <br/> Lancement de l'analyse)

    D --> E(Start service_capture)

    E --> F(Service_ipmac)
    F --> E
    E --> G(Service_protocol)
    G --> E
    E --> H(Service_autre)
    H --> E

    F --> I(data_on_dashboard)
    G --> I(data_on_dashboard)
    H --> I(data_on_dashboard)

    I --> J(Stop RPI)
``` 

## Arborescence 
```
├───scrat_app
│   ├───app_capture         --> Module de capture réseau
│   ├───app_ipmac           --> Module d'analyse des IP/MAC
│   ├───app_protocol        --> Module d'analyse des ports
│   ├───app_web             --> Module de l'IHM
│   │   ├───routes              --> Backend/Endpoints
│   │   ├───static              --> CSS, JS, Media
│   │   └───templates           --> Frontend 
│   ├───data                --> Persistance des données
│   │   ├───data_capture        --> Données des captures
│   │   ├───data_processing     --> Données des analyses
│   │   └───data_user           --> Données de configuration
│   └───utils               --> Fontions pythons
└───static                  --> Fichiers d'installations
```

## Debug des services
```bash
sudo journalctl -u scrat_app_web -e
sudo journalctl -u scrat_app_capture -e
sudo journalctl -u scrat_app_ipmac -e
sudo journalctl -u scrat_app_protocol -e
```
