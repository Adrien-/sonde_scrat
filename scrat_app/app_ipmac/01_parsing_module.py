import glob
import pyshark
import os
import csv
import time
import socket

import sys
sys.path.append("../")
from utils.lib import get_datetime_now, get_data_directory


data_capture_directory, data_processing_directory = get_data_directory()
date_str = get_datetime_now()


def get_hostname(ip_address):
    try:
        hostname = socket.gethostbyaddr(ip_address)[0]
        return hostname
    except socket.herror:
        return "N/A"
    except socket.timeout:
        return "Timeout"


# Recherche du dernier fichier pcap dans le répertoire
pcap_filename_pattern = "capture_*.pcap"
# Tant qu'aucun fichier n'est trouvé, attendre 20 secondes et réessayer
while not os.listdir(data_capture_directory):
    time.sleep(20)
    print("Attente du 1er fichier de capture")
pcap_files = glob.glob(data_capture_directory + pcap_filename_pattern)
latest_pcap_file = max(pcap_files, key=lambda f: os.path.getctime(f))

# Lire le fichier pcap avec pyshark
capture = pyshark.FileCapture(latest_pcap_file)

# Dictionnaire pour stocker les adresses IP, MAC et noms d'hôte uniques
addresses = {}

# Parcourir les paquets capturés
for packet in capture:
    try:
        ip_src = packet.ip.src
        mac_src = packet.eth.src
        ip_dst = packet.ip.dst
        mac_dst = packet.eth.dst

        # Résolution du nom d'hôte
        hostname_src = get_hostname(ip_src)
        hostname_dst = get_hostname(ip_dst)

        # Stocker les adresses IP, MAC et noms d'hôte dans le dictionnaire
        addresses[ip_src] = (mac_src, hostname_src)
        addresses[ip_dst] = (mac_dst, hostname_dst)
    except AttributeError:
        # Ignorer les paquets sans adresses IP ou MAC
        pass

# Afficher les adresses IP, MAC et noms d'hôte uniques
for ip, (mac, hostname) in addresses.items():
    print("IP:", ip, "| MAC:", mac, "| Hostname:", hostname)

# Écriture dans le fichier de log
output_file = f"{data_processing_directory}/out_ipmac_{date_str}.csv"
with open(output_file, "w", newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['IP', 'MAC', 'Hostname', 'Première détection', 'Dernière détection'])  # En-tête des colonnes
    for ip, (mac, hostname) in addresses.items():
        writer.writerow([ip, mac, hostname, date_str, date_str])

print("Fichier de sortie:", output_file)
