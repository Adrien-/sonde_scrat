import glob
import os
import csv
import shutil


import sys
sys.path.append("../")
from utils.lib import get_datetime_now, get_data_directory


data_capture_directory, data_processing_directory = get_data_directory()
date_str = get_datetime_now()





# Recherche des fichiers générés correspondant au modèle
filename_pattern = "out_ipmac_*.csv"
generated_files = glob.glob(data_processing_directory + filename_pattern)
# Tri des fichiers par ordre de modification (du plus récent au plus ancien)
generated_files.sort(key=os.path.getmtime, reverse=True)
# Sélection du dernier fichier généré
last_out_ipmac_csv = generated_files[0] if generated_files else None


# 1ere fois creation du global
global_ipmac_csv = data_processing_directory + "global_ipmac.csv"
if not os.path.exists(global_ipmac_csv):
    # Fichier global_ipmac.csv n'existe pas, effectuer une copie de out_ipmac*.csv
    shutil.copy(last_out_ipmac_csv, global_ipmac_csv)
    print("Copie effectuée : out_ipmac_*.csv -> global_ipmac.csv")
else:
    print("Le fichier global_ipmac.csv existe déjà.")


if last_out_ipmac_csv is None:
    print("Aucun fichier trouvé.")
else:
    print("Dernier fichier trouvé: " + last_out_ipmac_csv)

    new_lines_counter = 0  # Compteur de nouvelles lignes
    updated_lines_counter = 0 # Compteur de modification de lignes

    # Chemin du fichier global.csv
    global_csv_file = global_ipmac_csv
    # Chemin du fichier avec les nouvelles données
    new_data_file = last_out_ipmac_csv

    # Dictionnaire pour stocker les données du fichier global.csv
    global_data = {}

    # Lire le fichier global.csv et stocker les données dans le dictionnaire
    with open(global_csv_file, newline='') as file:
        reader = csv.reader(file)
        next(reader)  # Ignorer l'en-tête du fichier
        for row in reader:
            ip, mac, hostname, premiere_detection, derniere_detection = row
            global_data[(ip, mac)] = (hostname, premiere_detection, derniere_detection)

    # Lire le fichier avec les nouvelles données
    with open(new_data_file, newline='') as file:
        reader = csv.reader(file)
        next(reader)  # Ignorer l'en-tête du fichier
        for row in reader:
            ip, mac, hostname, premiere_detection, derniere_detection = row
            key = (ip, mac)
            if key in global_data:
                # Les données existent déjà dans le fichier global.csv, mettre à jour la dernière détection
                # FIXME: a debug   
                _, premiere_detection_old, _ = global_data[key]
                global_data[key] = (hostname, premiere_detection_old, derniere_detection)
                updated_lines_counter += 1
            else:
                # Les données sont nouvelles, les ajouter au fichier global.csv
                global_data[key] = (hostname, premiere_detection, derniere_detection)
                new_lines_counter += 1

    # Supprimer le contenu du fichier global.csv
    open(global_csv_file, 'w').close()

    # Écrire les données mises à jour dans le fichier global.csv
    with open(global_csv_file, 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["IP", "MAC", "Hostname", "Première détection", "Dernière détection"])  # Écrire l'en-tête
        for (ip, mac), (hostname, premiere_detection, derniere_detection) in global_data.items():
            writer.writerow([ip, mac, hostname, premiere_detection, derniere_detection])
        

    print("Nombre de nouvelles lignes ajoutees au fichier global_ipmac.csv: ", new_lines_counter)
    print("Nombre de lignes modifier au fichier global_ipmac.csv: ", updated_lines_counter)
