import glob
import pyshark
import os
import csv
import time


import sys
sys.path.append("../")
from utils.lib import get_datetime_now, get_data_directory


data_capture_directory, data_processing_directory = get_data_directory()
date_str = get_datetime_now()




# Recherche du dernier fichier pcap dans le repertoire
pcap_filename_pattern = "capture_*.pcap"
# Tant qu'aucun fichier n'est trouvé, attendre 20 secondes et réessayer
while not os.listdir(data_capture_directory):
    time.sleep(20)
    print("Attente du 1er fichier de capture")
pcap_files = glob.glob(data_capture_directory + pcap_filename_pattern)
latest_pcap_file = max(pcap_files, key=lambda f: os.path.getctime(f))

# Lire le fichier pcap avec pyshark
capture = pyshark.FileCapture(latest_pcap_file)

# Dictionnaire pour stocker les adresses IP et MAC uniques
addresses = {}

# Parcourir les paquets captures
for packet in capture:
    try:
        ip_src = packet.ip.src
        mac_src = packet.eth.src
        ip_dst = packet.ip.dst
        mac_dst = packet.eth.dst

        # Stocker les adresses IP et MAC dans le dictionnaire
        addresses[ip_src] = mac_src
        addresses[ip_dst] = mac_dst
    except AttributeError:
        # Ignorer les paquets sans adresses IP ou MAC
        pass

# Afficher les adresses IP et MAC uniques
for ip, mac in addresses.items():
    print("IP:", ip, "| MAC:", mac)

# Ecrit dans le fichier de log
with open(f"{data_processing_directory}/out_ipmac_{date_str}.csv", "w", newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['IP', 'MAC', 'Première détection', 'Dernière détection'])  # Écriture de l'en-tête des colonnes
    for ip, mac in addresses.items():
        writer.writerow([ip, mac, date_str, date_str])
