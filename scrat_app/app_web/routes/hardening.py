from flask import Blueprint, render_template, request
import subprocess
import os
import json
import csv
import re


import sys
sys.path.append("../../")
from scrat_app.utils.lib import get_datetime_now, get_data_directory, check_service_status, read_user_input_file




# Creation du blueprint pour l'endpoint hardening
hardening_route = Blueprint('hardening', __name__)

hardening_2_route = Blueprint('hardening_2_route', __name__)


historical_scans = []

# Route pour l'endpoint hardening
@hardening_route.route('/hardening')
def hardening():
    path_hist = '/home/scrat/sonde_scrat/scrat_app/data/data_user/historical_scans_harden.csv'
    scans = []
    try:
        with open(path_hist, 'r') as file:
            reader = csv.reader(file)
            header = next(reader)  # Lecture de l'en-tête
            for row in reader:
                scan_result = {'datetime': row[0], 'output': row[1]}
                scans.append(scan_result)
    except FileNotFoundError:
        pass
    return render_template('hardening.html', title="Sonde SCRAT", header=header, historical_scans=scans, label_status=check_service_status("scrat_app_capture.service"))

@hardening_route.route('/hardening')
def hardening():
    path_hist = '/home/scrat/sonde_scrat/scrat_app/data/data_user/historical_scans_harden.csv'
    scans = []
    try:
        with open(path_hist, 'r') as file:
            reader = csv.reader(file)
            header = next(reader)  # Lecture de l'en-tête
            for row in reader:
                if not row[0] and not row[1]:  # Vérification des valeurs vides
                    raise ValueError("Les colonnes datetime et output sont vides.")
                scan_result = {'datetime': row[0], 'output': row[1]}
                scans.append(scan_result)
    except FileNotFoundError:
        pass
    except ValueError as e:
        # error_message = str(e)
        scans="Pas encore de données"
        # Traitez l'erreur comme vous le souhaitez, par exemple, en l'affichant ou en la stockant dans une variable
    return render_template('hardening.html', title="Sonde SCRAT", header=header, historical_scans=scans, label_status=check_service_status("scrat_app_capture.service"))





@hardening_2_route.route('/scan_hardening', methods=['POST'])
def hardening_2():
    command = "lynis audit system | grep index"
    output = subprocess.check_output(command, shell=True).decode('utf-8')
   
    # output = output.strip() 
    output = output.replace('\033[0;36m', '').replace('\033[0m', '').replace('\033[1;37m', '').replace('\033[1;33m', '').replace('[', '').replace(']', '')

    scan_result = {'datetime': get_datetime_now(), 'output': output}
    historical_scans.append(scan_result)


    path_hist = '/home/scrat/sonde_scrat/scrat_app/data/data_user/historical_scans_harden.csv'
    file_exists = os.path.isfile(path_hist)
    with open(path_hist, 'a', newline='') as file:
        writer = csv.writer(file)
        if not file_exists:
            writer.writerow(['datetime', 'output'])  # Écriture de l'en-tête seulement si le fichier n'existe pas
        for scan_result in historical_scans:
            writer.writerow([scan_result['datetime'], scan_result['output']])

    scans = []
    try:
        with open(path_hist, 'r') as file:
            reader = csv.reader(file)
            header = next(reader)  # Lecture de l'en-tête
            for row in reader:
                scan_result = {'datetime': row[0], 'output': row[1]}
                scans.append(scan_result)
    except FileNotFoundError:
        pass


    # Masquer le spinner manuellement en appelant la fonction hideSpinner() en JavaScript
    hide_spinner_js = '<script>hideSpinner();</script>'
    return render_template('hardening.html', title="Sonde SCRAT", header=header, historical_scans=scans, label_status=check_service_status("scrat_app_capture.service")) + hide_spinner_js
