from flask import Blueprint, render_template
import csv
import os
import datetime



import sys
sys.path.append("../../")
from scrat_app.utils.lib import get_data_directory, check_service_status


# Création du blueprint pour l'endpoint adresse_ip
adress_ip_mac_route = Blueprint('adress_ip_mac', __name__)


# Route pour l'endpoint adresse_ip
@adress_ip_mac_route.route('/dashboard_ipmac')
def adresse_ip_mac():

    # Chemin du fichier
    data_capture_directory, data_processing_directory = get_data_directory()
    fichier_global_csv = data_processing_directory + "/global_ipmac.csv"

    

    if os.path.exists(fichier_global_csv):
        lignes = []
        with open(fichier_global_csv, 'r') as fichier:
            lecteur_csv = csv.reader(fichier)
            for ligne in lecteur_csv:
                lignes.append(ligne)

        # Récupération de la date de dernière exécution du script
        files = os.listdir(data_processing_directory)
        sorted_files = sorted(files, key=lambda x: os.path.getctime(os.path.join(data_processing_directory, x)))
        latest_file = sorted_files[-1]
        latest_file_path = os.path.join(data_processing_directory, latest_file)
        creation_time = os.path.getctime(latest_file_path)
        creation_datetime = datetime.datetime.fromtimestamp(creation_time)
        formatted_creation_date = creation_datetime.strftime("%Y-%m-%d %H:%M:%S")
    elif not os.path.exists(fichier_global_csv):
        lignes = []
        formatted_creation_date="Pas encore de donnée."

    return render_template('adress_ip_mac.html', title="Adress IP/MAC", lignes=lignes, last_execution_date=formatted_creation_date, label_status=check_service_status("scrat_app_capture.service"))
