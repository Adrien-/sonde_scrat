from flask import Blueprint, render_template, request
import subprocess
import os
from scapy.all import *
import glob
import pyshark
import os
import csv
import time
from collections import Counter


import sys
sys.path.append("../../")
from scrat_app.utils.lib import get_data_directory, get_datetime_now, check_service_status, read_user_input_file



# Fonction pour lire la liste de protocoles depuis le fichier CSV
def lire_liste_protocoles():
    protocoles = []
    filename = "/home/scrat/sonde_scrat/scrat_app/data/data_user/protocoles_autorises.csv"
    with open(filename, "r") as file:
        reader = csv.reader(file)
        for row in reader:
            protocoles.append(row[0])
    return sorted(protocoles, key=int)




# Creation du blueprint pour l'endpoint index
protocol_route = Blueprint('protocol', __name__)

# Route pour l'endpoint
@protocol_route.route('/dashboard_protocol', methods=["GET", "POST"])
def protocol():

    data_capture_directory, data_processing_directory = get_data_directory()
    date_str = get_datetime_now()


    if request.method == "POST":
        if "protocole" in request.form:
            # Ajout d'un nouveau protocole à la liste
            protocole = request.form.get("protocole")
            if not protocole:
                print("Veuillez saisir un protocole.")
            
            protocoles_existant = lire_liste_protocoles()
            if protocole in protocoles_existant:
                print(f"Le protocole {protocole} existe déjà dans la liste.")
            
            protocoles_existant.append(protocole)
            
            # Enregistrement de la liste de protocoles dans un fichier CSV
            filename = "/home/scrat/sonde_scrat/scrat_app/data/data_user/protocoles_autorises.csv"
            with open(filename, "w", newline="") as file:
                writer = csv.writer(file)
                for protocole in protocoles_existant:
                    writer.writerow([protocole])
            
        
        elif "supprimer" in request.form:
            # Suppression d'un protocole de la liste
            protocole_supprime = request.form.get("supprimer")
            protocoles_existant = lire_liste_protocoles()
            if protocole_supprime not in protocoles_existant:
                print(f"Le protocole {protocole_supprime} n'existe pas dans la liste.")
            
            protocoles_existant.remove(protocole_supprime)
            
            # Enregistrement de la liste de protocoles mise à jour dans un fichier CSV
            filename = "/home/scrat/sonde_scrat/scrat_app/data/data_user/protocoles_autorises.csv"
            with open(filename, "w", newline="") as file:
                writer = csv.writer(file)
                for protocole in protocoles_existant:
                    writer.writerow([protocole])
    else:
        protocoles_existant = lire_liste_protocoles()

    
    # Chemin vers le fichier CSV
    file_path = data_processing_directory + "global_protocol.csv"

    # Vérification de l'existence du fichier
    if os.path.exists(file_path):

        # Liste pour stocker les lignes du fichier CSV
        lignes = []

        # Lecture du fichier CSV
        with open(file_path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)
            header = next(csv_reader)  # Lecture de l'en-tête

            # Ajout de l'en-tête au tableau de données
            lignes.append(header)

            # Ajout des lignes de données au tableau
            for row in csv_reader:
                lignes.append(row)
    else:
        print("Pas encore de donnée.")
        lignes=""

    return render_template('protocol.html', title="Protocol", lignes=lignes, protocoles=lire_liste_protocoles(), last_execution_date="", label_status=check_service_status("scrat_app_capture.service"))




