from flask import Blueprint, render_template, request
import plotly.graph_objects as go
import pandas as pd
import subprocess
import os
import json
import csv

import sys
sys.path.append("../../")
from scrat_app.utils.lib import get_data_directory, get_datetime_now, check_service_status, read_user_input_file


# Création du blueprint pour l'endpoint adresse_ip
kpi_route = Blueprint('kpi', __name__)


#FF6D60 rouge
#F7D060 jaune
#F3E99F orange
#98D8AA vert

# Route pour l'endpoint adresse_ip
@kpi_route.route('/kpi')
def kpi():

    data_capture_directory, data_processing_directory = get_data_directory()
    date_str = get_datetime_now()

# PARTIE IP/MAC
    # Vérifier l'existence du fichier
    if os.path.isfile(data_processing_directory + "global_ipmac.csv"):
        # Charger les données depuis le fichier CSV
        df = pd.read_csv(data_processing_directory + "global_ipmac.csv")

        # Compter le nombre de lignes par date de première détection
        counts = df['Première détection'].value_counts().sort_index()

        # Obtenir les valeurs uniques de Première détection et leurs compteurs
        unique_dates = counts.index.tolist()
        y_values = counts.values.tolist()

        # Créer l'histogramme avec plotly.graph_objects
        fig = go.Figure(data=[go.Bar(x=unique_dates, y=y_values, marker=dict(color='#98D8AA'))])


        # Configurer les étiquettes des axes
        fig.update_layout(
            # title="Graphique IP/MAC",
            xaxis_title='Date de première détection',
            yaxis_title='Nombre d\'IP',
            plot_bgcolor='rgba(0,0,0,0)',
            margin=dict(l=0, r=0, t=0, b=0)
        )
        
        # Convertir la figure en JSON
        # graphJSON01 = fig.to_json()
        plot_html_01 = fig.to_html(full_html=False)

        # Afficher le graphique
        # fig.show()
        info = ""
    else:
        plot_html_01=""
        info = "Pas encore de donnée."



# PARTIE PROTOCOL 01

    # Vérifier l'existence du fichier
    if os.path.isfile(data_processing_directory + "global_protocol.csv"):
        # Charger les données depuis le fichier CSV
        df = pd.read_csv(data_processing_directory + "global_protocol.csv")


        # Compter le nombre de nouvelles entrées par date
        counts = df['Première détection'].value_counts().sort_index()

        # Obtenir les dates uniques et leurs compteurs
        unique_dates = counts.index.tolist()
        y_values = counts.values.tolist()

        # Créer le graphique à barres avec plotly.graph_objects
        fig = go.Figure(data=[go.Bar(x=unique_dates, y=y_values, marker=dict(color='#98D8AA'))])

        # Configurer les étiquettes des axes
        fig.update_layout(
            xaxis_title='Date de première détection',
            yaxis_title='Nombre de nouvelles entrées',
            plot_bgcolor='rgba(0, 0, 0, 0)',
            margin=dict(l=0, r=0, t=0, b=0)
        )

        # Convertir la figure en HTML
        plot_html_02 = fig.to_html(full_html=False)

        # Afficher le graphique
        info2 = ""
    else:
        plot_html_02 = ""
        info2 = "Pas encore de données."

# PARTIE PROTOCOL 02

    if os.path.isfile(data_processing_directory + "global_protocol.csv"):
        # Charger les données depuis le fichier CSV
        file_path = data_processing_directory + "global_protocol.csv"

        # Dictionnaire pour stocker le nombre de fois qu'un port apparaît
        port_counts = {}

        # Lecture du fichier CSV
        with open(file_path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)
            header = next(csv_reader)  # Lecture de l'en-tête

            # Parcours des lignes de données
            for row in csv_reader:
                src_port = row[1]
                dst_port = row[2]

                # Comptage des ports source
                if src_port in port_counts:
                    port_counts[src_port] += 1
                else:
                    port_counts[src_port] = 1

                # Comptage des ports de destination
                if dst_port in port_counts:
                    port_counts[dst_port] += 1
                else:
                    port_counts[dst_port] = 1

        # Création du dataframe à partir du dictionnaire de comptage
        df = pd.DataFrame({'Port': list(port_counts.keys()), 'Count': list(port_counts.values())})

        # Tri du dataframe par compte décroissant
        df = df.sort_values('Count', ascending=False)

        # Création du graphique Plotly
        fig = go.Figure(data=[go.Bar(y=df['Port'], x=df['Count'], orientation='h')])

        # Mise en forme du graphique
        fig.update_layout(
            title='Nombre d\'occurrences des ports',
            xaxis_title='Nombre d\'occurrences',
            yaxis_title='Port',
            bargap=0.1,
            plot_bgcolor='rgba(0,0,0,0)'
        )

        # Convertir la figure en HTML
        plot_html_03 = fig.to_html(full_html=False)

        info3 = ""
    else:
        plot_html_03 = ""
        info3 = "Pas encore de données."




    # return render_template('kpi.html',  title="Sonde SCRAT", info=info, graphJSON01=graphJSON01)
    return render_template('kpi.html',  title="Sonde SCRAT", info=info, plot_html_01=plot_html_01, info2=info2, plot_html_02=plot_html_02,  info3=info3, plot_html_03=plot_html_03, label_status=check_service_status("scrat_app_capture.service"))

