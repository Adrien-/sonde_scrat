from flask import Blueprint, render_template, request
import subprocess
import os


import sys
sys.path.append("../../")
from scrat_app.utils.lib import get_data_directory, check_service_status, read_user_input_file




# Creation du blueprint pour l'endpoint index
index_route = Blueprint('index', __name__)

# Route pour l'endpoint index
@index_route.route('/')
def index():

    user_input = read_user_input_file()

    service_statuses = ' | '.join(f"{service}:{check_service_status(service)}" for service in ["scrat_app_web.service", "scrat_app_capture.service", "scrat_app_ipmac.service", "scrat_app_protocol.service"])

    return render_template('index.html', title="Sonde SCRAT", etat=service_statuses, reseau_name=user_input, label_status=check_service_status("scrat_app_capture.service"))




@index_route.route('/start_analysis', methods=['POST'])
def start_parser():

    # Enregistrer la saisie utilisateur dans un fichier texte
    user_input = request.form['user_input']
    user_input = user_input.replace(' ', '_').lower() # Remplace les espaces par des underscores + lower
    with open('/home/scrat/sonde_scrat/scrat_app/data/data_user/user_input.txt', 'w') as file:
        file.write(user_input)

    # Création du dossier en utilisant le contenu du fichier user_input.txt comme nom
    data_capture_directory, data_processing_directory = get_data_directory()


    # Vérification si le dossier existe déjà
    if not os.path.exists(data_capture_directory):
        # Création du dossier
        os.makedirs(data_capture_directory)
        print(f"Dossier '{user_input}' créé avec succès.")
    else:
        print(f"Dossier '{user_input}' existe déjà.")

    # Vérification si le dossier existe déjà
    if not os.path.exists(data_processing_directory):
        # Création du dossier
        os.makedirs(data_processing_directory)
        print(f"Dossier '{user_input}' créé avec succès.")
    else:
        print(f"Dossier '{user_input}' existe déjà.")


    subprocess.run(['sudo', 'systemctl', 'restart', 'scrat_app_capture.service'])
    subprocess.run(['sudo', 'systemctl', 'restart', 'scrat_app_ipmac.service'])
    subprocess.run(['sudo', 'systemctl', 'restart', 'scrat_app_protocol.service'])



    service_statuses = ' | '.join(f"{service}:{check_service_status(service)}" for service in ["scrat_app_web.service", "scrat_app_capture.service", "scrat_app_ipmac.service", "scrat_app_protocol.service"])


    return render_template('index.html', title="Sonde SCRAT", etat=service_statuses, reseau_name=user_input, label_status=check_service_status("scrat_app_capture.service"))
