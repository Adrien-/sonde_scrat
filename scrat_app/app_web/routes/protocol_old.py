from flask import Blueprint, render_template, request
import subprocess
import os
from scapy.all import *
import glob
import pyshark
import os
import csv
import time
from collections import Counter


import sys
sys.path.append("../../")
from scrat_app.utils.lib import get_data_directory, get_datetime_now, check_service_status, read_user_input_file




# Creation du blueprint pour l'endpoint index
protocol_route = Blueprint('protocol', __name__)

# Route pour l'endpoint
@protocol_route.route('/dashboard_protocol')
def protocol():


# FIXME: TODO: EN TEST -> faire un service apres si debut de fonctionnel
    data_capture_directory, data_processing_directory = get_data_directory()
    date_str = get_datetime_now()

    # Recherche du dernier fichier pcap dans le repertoire
    pcap_filename_pattern = "capture_*.pcap"
    # Tant qu'aucun fichier n'est trouvé, attendre 20 secondes et réessayer
    while not os.listdir(data_capture_directory):
        time.sleep(20)
        print("Attente du 1er fichier de capture")
    pcap_files = glob.glob(data_capture_directory + pcap_filename_pattern)
    latest_pcap_file = max(pcap_files, key=lambda f: os.path.getctime(f))


    def extract_protocols(file_path):
        packets = rdpcap(file_path)  # Charger le fichier de capture
        
        protocols = set()  # Utiliser un ensemble pour éviter les doublons
        
        for packet in packets:
            protocols.add(packet.payload.name)  # Ajouter le nom du protocole à l'ensemble
        
        return protocols

    # Exemple d'utilisation
    pcap_file = latest_pcap_file
    protocols = extract_protocols(pcap_file)

    lignes = []
    print("Protocoles extraits :")
    for protocol in protocols:
        print(protocol)
        lignes.append(protocol)

    # # Générer le contenu de la table HTML
    # table_html = "<table>"
    # for ligne in lignes:
    #     table_html += "<tr>"
    #     table_html += f"<td>{ligne}</td>"
    #     table_html += "</tr>"
    # table_html += "</table>"

    # # Afficher le contenu de la table dans la page HTML
    # print(table_html)




    formatted_creation_date = "A FAIRE"








    # TOP protocole


    # Chemin vers le fichier pcap
    pcap_file = latest_pcap_file

    # Lire le fichier pcap avec pyshark
    capture = pyshark.FileCapture(pcap_file)

    # Liste pour stocker les protocoles
    protocols = []

    # Parcourir les paquets capturés
    for packet in capture:
        try:
            # Récupérer le protocole du paquet
            protocol = packet.highest_layer

            # Stocker le protocole dans la liste
            protocols.append(protocol)
        except AttributeError:
            # Ignorer les paquets sans protocole
            pass

    # Compter les occurrences des protocoles
    protocol_counts = Counter(protocols)

    # Tableau pour stocker les résultats
    result_table = []

    # Ajouter les protocoles et leurs occurrences au tableau
    for protocol, count in protocol_counts.most_common(5):  # Modifier le nombre pour afficher plus ou moins de protocoles
        result_table.append((protocol, count))

    # Afficher le tableau
    print(result_table)


    return render_template('protocol.html', title="Protocol", lignes=lignes, last_execution_date=formatted_creation_date, top=result_table)


