//###############################
// PARTIE NEW
//###############################
// Récupérer toutes les cellules du tableau
var cells = document.querySelectorAll('table td:nth-child(3), table td:nth-child(4)');

// Parcourir les cellules et vérifier les valeurs de premiere_detection et derniere_detection
cells.forEach(function(cell) {
  var premiereDetection = cell.innerText;
  var derniereDetection = cell.nextElementSibling ? cell.nextElementSibling.innerText : null;
  

// FIXME: si est detecter qu'une fois en vert jusqu'a passer en orange --> rajouter un timeout ?
//  // recup la date
//   var [date, time] = cell.innerText.split('_');
//   var derniereDetectionFormatted = date + ' ' + time.replace(/-/g, ':');
//   var derniereDetectionDate = new Date(derniereDetectionFormatted);
//   var now = new Date();
//   var differenceInMinutes = (now - derniereDetectionDate) / (1000 * 60);
//   // Comparer les valeurs et appliquer le style vert si elles sont égales ET inf a 3min
//   if (premiereDetection === derniereDetection && differenceInMinutes > 3) {
//     cell.style.color = 'green';
//   }


  // Comparer les valeurs et appliquer le style vert si elles sont égales
  if (premiereDetection === derniereDetection) {
    cell.style.color = 'green';
  }
});




//###############################
// PARTIE INACTIVE
//###############################
// Récupérer toutes les cellules du tableau
var cells = document.querySelectorAll('table td:nth-child(4)');

// Parcourir les cellules et vérifier les valeurs de "derniereDetection"
cells.forEach(function(cell) {
  var derniereDetection = cell.innerText;

  // Séparer la date et l'heure
  var [date, time] = derniereDetection.split('_');

  // Remplacer les tirets par des deux-points dans le temps
  time = time.replace(/-/g, ':');

  // Concaténer la date et l'heure au format compatible avec la création d'un objet Date
  var derniereDetectionFormatted = date + ' ' + time;

  // Vérifier si la derniereDetection est supérieure à 5 minutes
  var derniereDetectionDate = new Date(derniereDetectionFormatted);
  var now = new Date();
  var differenceInMinutes = (now - derniereDetectionDate) / (1000 * 60);

  // Appliquer le style orange si la derniereDetection est supérieure à 15 minutes
  if (differenceInMinutes > 15) {
    cell.style.color = 'orange';
  }
});

