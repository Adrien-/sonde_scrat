// Récupérer la référence du bouton
var refreshButton = document.querySelector('.refresh-button');

// Ajouter un gestionnaire d'événement au clic sur le bouton
refreshButton.addEventListener('click', function() {
  // Rafraîchir la page
  location.reload();
});