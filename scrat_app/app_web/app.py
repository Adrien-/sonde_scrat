from flask import Flask

# App Flask
app = Flask(__name__)


# Import des routes
from routes.index import index_route
from routes.adress_ip_mac import adress_ip_mac_route
from routes.protocol import protocol_route
from routes.kpi import kpi_route
from routes.hardening import hardening_route
from routes.hardening import hardening_2_route


# Enregistrement des routes
app.register_blueprint(index_route)
app.register_blueprint(adress_ip_mac_route)
app.register_blueprint(protocol_route)
app.register_blueprint(kpi_route)
app.register_blueprint(hardening_route)
app.register_blueprint(hardening_2_route)




if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
