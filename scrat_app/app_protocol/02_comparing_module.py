import glob
import os
import csv
import shutil


import sys
sys.path.append("../")
from utils.lib import get_datetime_now, get_data_directory


data_capture_directory, data_processing_directory = get_data_directory()
date_str = get_datetime_now()





# Recherche des fichiers générés correspondant au modèle
filename_pattern = "out_protocol_*.csv"
generated_files = glob.glob(data_processing_directory + filename_pattern)
# Tri des fichiers par ordre de modification (du plus récent au plus ancien)
generated_files.sort(key=os.path.getmtime, reverse=True)
# Sélection du dernier fichier généré
last_out_protocol_csv = generated_files[0] if generated_files else None






if last_out_protocol_csv is None:
    print("Aucun fichier trouvé.")
else:
    # 1ere fois creation du global
    global_protocol_csv = data_processing_directory + "global_protocol.csv"
    if not os.path.exists(global_protocol_csv):
        # Fichier global_protocol_csv n'existe pas, effectuer une copie de out_ipmac*.csv
        shutil.copy(last_out_protocol_csv, global_protocol_csv)
        print("Copie effectuée : out_protocol_*.csv -> global_protocol.csv")
    else:
        print("Le fichier global_protocol.csv existe déjà.")


    print("Dernier fichier trouvé: " + last_out_protocol_csv)

    new_lines_counter = 0  # Compteur de nouvelles lignes

    # Chemin du fichier global.csv
    global_csv_file = global_protocol_csv
    # Chemin du fichier avec les nouvelles données
    new_data_file = last_out_protocol_csv

    # Dictionnaire pour stocker les données du fichier global.csv
    global_data = {}

    # Lire le fichier global.csv et stocker les données dans le dictionnaire
    with open(global_csv_file, newline='') as file:
        reader = csv.reader(file)
        next(reader)  # Ignorer l'en-tête du fichier
        for row in reader:
            premiere_detection, src_port, dst_port, = row
            global_data[(src_port, dst_port)] = (premiere_detection)

    # Lire le fichier avec les nouvelles données
    with open(new_data_file, newline='') as file:
        reader = csv.reader(file)
        next(reader)  # Ignorer l'en-tête du fichier
        for row in reader:
            premiere_detection, src_port, dst_port = row
            key = (src_port, dst_port)
            # Les données sont nouvelles, les ajouter au fichier global.csv
            global_data[key] = (premiere_detection)
            new_lines_counter += 1

    # Supprimer le contenu du fichier global.csv
    open(global_csv_file, 'w').close()

    # Écrire les données mises à jour dans le fichier global.csv
    with open(global_csv_file, 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["Première détection", "src_port", "dst_port"])  # Écrire l'en-tête
        for (src_port, dst_port), (premiere_detection) in global_data.items():
            writer.writerow([premiere_detection, src_port, dst_port])
        

    print("Nombre de nouvelles lignes ajoutees au fichier global_protocol_csv: ", new_lines_counter)
