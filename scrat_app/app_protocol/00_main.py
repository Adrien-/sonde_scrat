import time
import subprocess

while True:
    print("## MODULE PROTOCOL | 01: Parsing...")
    # Execution du script 02_processing_module.py
    subprocess.run(["python3", "01_parsing_module.py"])

    print("## MODULE PROTOCOL | 02: Comparing...")
    # Execution du script 03_comparison_module.py
    subprocess.run(["python3", "02_comparing_module.py"])

    print("## Sleep...")
    # Attente de x secondes
    time.sleep(20)
