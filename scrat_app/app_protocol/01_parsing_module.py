import glob
import pyshark
import os
import csv
import time
import socket


import sys
sys.path.append("../")
from utils.lib import get_datetime_now, get_data_directory


data_capture_directory, data_processing_directory = get_data_directory()
date_str = get_datetime_now()

# Recherche du dernier fichier pcap dans le répertoire
pcap_filename_pattern = "capture_*.pcap"
# Tant qu'aucun fichier n'est trouvé, attendre 20 secondes et réessayer
while not os.listdir(data_capture_directory):
    time.sleep(20)
    print("Attente du 1er fichier de capture")
pcap_files = glob.glob(data_capture_directory + pcap_filename_pattern)
latest_pcap_file = max(pcap_files, key=lambda f: os.path.getctime(f))

# Lire le fichier pcap avec pyshark
capture = pyshark.FileCapture(latest_pcap_file)


# protocoles_autorises = {80, 443, 22, 20, 21, 123, 53, 67, 3389}
# Charger la liste des protocoles autorisés à partir du fichier CSV
protocoles_autorises = set()
protocoles_detectes = set()
with open("/home/scrat/sonde_scrat/scrat_app/data/data_user/protocoles_autorises.csv", "r") as file:
    reader = csv.reader(file)
    for row in reader:
        protocoles_autorises.add(str(row[0]))

protocoles_str = ', '.join(str(protocole) for protocole in protocoles_autorises)
print("Protocoles autorisés :", protocoles_str)


for packet in capture:
    if 'tcp' in packet:
        src_port = packet.tcp.srcport
        dst_port = packet.tcp.dstport
    elif 'udp' in packet:
        src_port = packet.udp.srcport
        dst_port = packet.udp.dstport
    else:
        continue
        
    if str(src_port) not in str(protocoles_autorises) and str(dst_port) not in str(protocoles_autorises):
        protocoles_detectes.add((src_port, dst_port))
    

if protocoles_detectes:
    with open(f"{data_processing_directory}/out_protocol_{date_str}.csv", "a", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(['src_port', 'dst_port', 'Première détection', 'Dernière détection'])  # En-tête des colonnes
        for src_port, dst_port in protocoles_detectes:
            writer.writerow([date_str, src_port, dst_port])
    print("Protocoles non autorisés détectés. Les informations ont été enregistrées dans le fichier CSV.")
else:
    print("Aucun protocole non autorisé détecté.")

