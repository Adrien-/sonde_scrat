import subprocess
import os


import sys
sys.path.append("../")
from utils.lib import get_datetime_now, get_data_directory, read_user_input_file


# Si run alors que pas de nom de reseau via l'utilisateur
user_input = read_user_input_file()
if not user_input:
    exit()


data_capture_directory, data_processing_directory = get_data_directory()
date_str = get_datetime_now()


# Permission sur le dossier data_capture_directory
permission_cmd = [
    "sudo",
    "chmod",
    "777",
    data_capture_directory
]
subprocess.run(permission_cmd)


# Chemin complet du fichier pcap
capture_file_name = data_capture_directory + f"capture_{date_str}.pcap"



# Interface
interface = "eth0"

# Duree la capture en secondes
# Intialement la premiere capture = 3min PUIS 1min tout le temps
if not os.listdir(data_capture_directory): # Le dossier data est vide.
    capture_duration = 60 # 3 min
else: # Le dossier data n'est pas vide.
    capture_duration = 60



# Commande pour effectuer la capture via Tshark
tshark_cmd = [
    "sudo",
    "tshark",
    "-i", interface,
    "-a", f"duration:{capture_duration}",
    "-w", capture_file_name
]


print("Debut de la capture...")

# Run de la commande Tshark pour effectuer la capture
subprocess.run(tshark_cmd)

print("Capture termine. Fichier enregistre: ", capture_file_name)
