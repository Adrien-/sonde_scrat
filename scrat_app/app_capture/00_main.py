import time
import subprocess

while True:
    # Execution du script 01_capture_module.py
    print("## MODULE CAPTURE | 01: Scan...")
    subprocess.run(["python3", "01_capture_module.py"])

    print("## Sleep...")
    # Attente de x secondes
    time.sleep(60)
