import datetime
import subprocess



def get_datetime_now():
    """
    Usage : date_str = get_datetime_now()
    """
    return  datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")



def read_user_input_file():
    """
    Usage : user_input = read_user_input_file()
    """
    file_path = '/home/scrat/sonde_scrat/scrat_app/data/data_user/user_input.txt'
    with open(file_path, 'r') as file:
        user_input = file.read().strip()
    return user_input



def get_data_directory():
    """
    Usage : data_capture_directory, data_processing_directory = get_data_directory()
    """
    user_input = read_user_input_file()
    data_capture_directory = "/home/scrat/sonde_scrat/scrat_app/data/data_capture_" + user_input + "/"
    data_processing_directory = "/home/scrat/sonde_scrat/scrat_app/data/data_processing_" + user_input + "/"
    return data_capture_directory, data_processing_directory



def check_service_status(service_name):
    """
    Usage : service_statuses = '|'.join(check_service_status(service) for service in ["scrat_app_web.service", "scrat_app_capture.service"])
    """
    command = ["systemctl", "is-active", service_name]
    try:
        output = subprocess.check_output(command, stderr=subprocess.STDOUT)
        return output.decode().strip()
    except subprocess.CalledProcessError as e:
        return e.output.decode().strip()