#!/bin/bash
# Projet : Sonde SCRAT - M2 SSIR
#
# Version : 2023/06/17

echo "###########################"

echo "## STEP 1 - Install Python + dependance"
# Installation de Python
apt update
apt install -y python3
# Installation de pip (gestionnaire de paquets Python)
apt install -y python3-pip
# Installation des packages Python à partir de requirements.txt
pip3 install -r requirements.txt
# Vérification de l'installation
python3 --version
pip3 --version

# tshark
apt install tshark -y

# lynis
apt install lynis -y



echo "## STEP 2 - Install file service SCRAT"

# FILE STATIC
FILE_0="/etc/rc.local"
SERVICE_FILE_1="/etc/systemd/system/scrat_app_web.service"
SERVICE_FILE_2="/etc/systemd/system/scrat_app_capture.service"
SERVICE_FILE_3="/etc/systemd/system/scrat_app_ipmac.service"
SERVICE_FILE_4="/etc/systemd/system/scrat_app_protocol.service"


# Vérifier si le fichier existe déjà
if [[ -f $FILE_0 ]]; then
  echo "Le fichier $FILE_0 existe déjà."
  $FILE_0_backup = $FILE_0 + "_backup"
  cp $FILE_0 $FILE_0_backup
  rm $FILE_0
fi
# Vérifier si le fichier existe déjà
if [[ -f $SERVICE_FILE_1 ]]; then
  echo "Le fichier $SERVICE_FILE_1 existe déjà."
  $SERVICE_FILE_1_backup = $SERVICE_FILE_1 + "_backup"
  cp $SERVICE_FILE_1 $SERVICE_FILE_1_backup
  rm $SERVICE_FILE_1
fi
# Vérifier si le fichier existe déjà
if [[ -f $SERVICE_FILE_2 ]]; then
  echo "Le fichier $SERVICE_FILE_2 existe déjà."
  $SERVICE_FILE_2_backup = $SERVICE_FILE_2 + "_backup"
  cp $SERVICE_FILE_2 $SERVICE_FILE_2_backup
  rm $SERVICE_FILE_2
fi
# Vérifier si le fichier existe déjà
if [[ -f $SERVICE_FILE_3 ]]; then
  echo "Le fichier $SERVICE_FILE_3 existe déjà."
  $SERVICE_FILE_3_backup = $SERVICE_FILE_3 + "_backup"
  cp $SERVICE_FILE_3 $SERVICE_FILE_3_backup
  rm $SERVICE_FILE_3
fi
# Vérifier si le fichier existe déjà
if [[ -f $SERVICE_FILE_4 ]]; then
  echo "Le fichier $SERVICE_FILE_4 existe déjà."
  $SERVICE_FILE_4_backup = $SERVICE_FILE_4 + "_backup"
  cp $SERVICE_FILE_4 $SERVICE_FILE_4_backup
  rm $SERVICE_FILE_4
fi


# Écrire le contenu du fichier
cat <<EOF > $FILE_0

#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

# Print the IP address
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi


# /etc/rc.local
sudo systemctl restart scrat_app_web.service
sudo systemctl stop scrat_app_capture.service
sudo systemctl stop scrat_app_ipmac.service
sudo systemctl stop scrat_app_protocol.service


exit 0
EOF
echo "Le fichier $FILE_0 a été créé avec succès."

# Écrire le contenu du fichier
cat <<EOF > $SERVICE_FILE_1
[Unit]
Description=scrat_app_web

[Service]
ExecStart=/usr/bin/sudo /usr/bin/python /home/scrat/sonde_scrat/scrat_app/app_web/app.py
WorkingDirectory=/home/scrat/sonde_scrat/scrat_app/app_web/
StandardError=journal
SyslogIdentifier=scrat_app_web
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOF
echo "Le fichier $SERVICE_FILE_1 a été créé avec succès."

# Écrire le contenu du fichier
cat <<EOF > $SERVICE_FILE_2
[Unit]
Description=scrat_app_capture

[Service]
ExecStart=/usr/bin/sudo /usr/bin/python /home/scrat/sonde_scrat/scrat_app/app_capture/00_main.py
WorkingDirectory=/home/scrat/sonde_scrat/scrat_app/app_capture/
StandardError=journal
SyslogIdentifier=scrat_app_capture
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOF
echo "Le fichier $SERVICE_FILE_2 a été créé avec succès."

# Écrire le contenu du fichier
cat <<EOF > $SERVICE_FILE_3
[Unit]
Description=scrat_app_ipmac

[Service]
ExecStart=/usr/bin/sudo /usr/bin/python /home/scrat/sonde_scrat/scrat_app/app_ipmac/00_main.py
WorkingDirectory=/home/scrat/sonde_scrat/scrat_app/app_ipmac/
StandardError=journal
SyslogIdentifier=scrat_app_ipmac
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOF
echo "Le fichier $SERVICE_FILE_3 a été créé avec succès."

# Écrire le contenu du fichier
cat <<EOF > $SERVICE_FILE_4
[Unit]
Description=scrat_app_protocol

[Service]
ExecStart=/usr/bin/sudo /usr/bin/python /home/scrat/sonde_scrat/scrat_app/app_protocol/00_main.py
WorkingDirectory=/home/scrat/sonde_scrat/scrat_app/app_protocol/
StandardError=journal
SyslogIdentifier=scrat_app_protocol
Restart=always
User=root

[Install]
WantedBy=multi-user.target
EOF
echo "Le fichier $SERVICE_FILE_4 a été créé avec succès."



systemctl daemon-reload
sudo systemctl enable scrat_app_web.service



echo "## STEP 3 - Reboot"
shutdown -r now
